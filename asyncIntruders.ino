#include "LedControl.h"
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

const unsigned int numDisplays = 4;
const unsigned int intensity = 1;

// BOARD CONNECTIONS
// Orange - MAX7219 Pin GND | Ardunio Pin GND
// Red -    MAX7219 Pin CLK | Ardunio Pin 13
// Green -  MAX7219 Pin DIN | Ardunio Pin 11
// Yellow - MAX7219 Pin VCC | Ardunio Pin 5V
// Brown -  MAX7219 Pin CS  | Ardunio Pin 10
LedControl lc = LedControl(11, 13, 10, numDisplays); // Pins: DIN,CLK,CS, # of Display connected

static const unsigned long REFRESH_INTERVAL = 10000; // ms
static const unsigned long UPDATE_INTERVAL = 500;

unsigned long currentMillis = 0;    // stores the value of millis() in each iteration of loop()
unsigned int currentLevel = 0;

int positions[4] = {0, 1, 2, 3};

struct sPuzzle {
  const unsigned int level;
  const char *       secret;
  const char *       key;
  const char *       encodedMessage;
  byte               complete;
  unsigned int       guesses;
  const char *       hint;
};


sPuzzle puzzles[] = {
  { 0, "STEM", "Q18", "QRCK", LOW, 0, "Use the wheel" },
  { 1, "CRYPTOGRAPHY", "H16", "HWDUYTLWFUMD", LOW, 0, "Use the wheel" },
  { 2, "SPACE INVADERS", "O6", "URCEG KPXCFGTU", LOW, 0, "Double encoded" }, //GDOQS WBJORSFG
  { 3, "YOU WIN", "E23", "RMA YSC", LOW, 0, "Double encoded and..." } //CXL JDN
};

struct sInvader {
  unsigned int        position;   // matrix position
  const unsigned int  puzzle;     // puzzle number
  const unsigned long interval;   // time delay for this
  byte                state;      // state (LOW/HIGH)
  unsigned long       previous;   // previous milli
  byte                dead;       // is this invader dead?
  byte                high[8];    // high state LCD position
  byte                low[8];     // low state LCD position
};

sInvader invaders[] = {
  { 0, 0, 200, LOW, 0 , LOW,
    {
      B00011000,
      B00111100,
      B01111110,
      B11011011,
      B11111111,
      B00100100,
      B01011010,
      B01000010
    },
    {
      B00011000,
      B00111100,
      B01111110,
      B11011011,
      B11111111,
      B00100100,
      B01011010,
      B10100101
    }
  },
  { 1, 1, 400, LOW, 0, LOW,
    {
      B00100100,
      B00100100,
      B01111110,
      B11011011,
      B11111111,
      B11111111,
      B10100101,
      B00100100
    },
    {
      B00100100,
      B10100101,
      B11111111,
      B11011011,
      B11111111,
      B01111110,
      B00100100,
      B01000010
    }
  },
  { 2, 2, 100, LOW, 0, LOW,
    {
      B10000001,
      B01000010,
      B00100100,
      B00011000,
      B10111110,
      B01111010,
      B00111110,
      B00100100
    },
    {
      B00100100,
      B00011000,
      B00011000,
      B00011000,
      B00111110,
      B11110110,
      B00111110,
      B00100100
    }
  },
  { 3, 3, 800, LOW, 0, LOW,
    {
      B00111000,
      B00001000,
      B00111100,
      B00100100,
      B11111111,
      B10000001,
      B11111111,
      B01011010
    },
    {
      B00001110,
      B00001000,
      B00111100,
      B00100100,
      B11111111,
      B10000001,
      B11111111,
      B01100110
    }
  }
};

// X on LCD
byte dead[8] = {
  B10000001,
  B01000010,
  B00100100,
  B00011000,
  B00011000,
  B00100100,
  B01000010,
  B10000001
};

// reset the game
void reset() {
  // make all the invaders alive
  for (int i = 0; i < ARRAY_SIZE(invaders); i++) {
    invaders[i].dead = LOW;
  }

  // clear all the puzzles and guesses
  for (int i = 0; i < ARRAY_SIZE(puzzles); i++) {
    puzzles[i].complete = LOW;
    puzzles[i].guesses = 0;
  }

  // set the level to 0 and time played to 0
  currentMillis = 0;
  currentLevel = 0;

  // print the level
  printLevel();
}

// position the invader on the LCD
void invade(int rowNum, byte* invader) {
  for (int i = 0; i < 8; i++) {
    lc.setRow(rowNum, i, invader[i]);
  }
}

// update all of the invaders state
void updateInvaders() {
 for (int i = 0; i < ARRAY_SIZE(invaders); i++) {
    updateInvader(invaders[i]);
  };  
}

// update individual invaders state
void updateInvader(sInvader &invader) {
  // 'move the invader'
  switch (invader.state) {
    case LOW:
      if (currentMillis - invader.previous >= invader.interval) {
        invader.state = HIGH;
        invader.previous += invader.interval;
      }
      break;

    case HIGH:
      if (currentMillis - invader.previous >= UPDATE_INTERVAL) {
        invader.state = LOW;
        invader.previous += UPDATE_INTERVAL;
      }
      break;

    default:
      break;
  }

  // check if they are dead
  if(puzzles[invader.puzzle].complete == HIGH){
    invader.dead = HIGH;
  }
}

// shuffle location of the invader on the LCD
void shuffleArray(int * array, int size) {
  int last = 0;
  int temp = array[last];
  for (int i = 0; i < size; i++) {
    int index = random(size);
    array[last] = array[index];
    last = index;
  }
  array[last] = temp;
}

void moveInvaders() {
  for (int i = 0; i < ARRAY_SIZE(invaders); i++) {
    // check if they are dead or alive and make them dance ore die
    switch (invaders[i].dead) {
      case LOW:
        invade(invaders[i].position, invaders[i].state == LOW ? invaders[i].high : invaders[i].low);
        break;
      case HIGH:
        invade(invaders[i].position, dead);
        break;
      default:
        break;
    }
  }

  // move the invaders to different locations on the LCD
  static unsigned long lastRefreshTime = 0;
  
  if (currentMillis - lastRefreshTime >= REFRESH_INTERVAL) {
    lastRefreshTime += REFRESH_INTERVAL;
    shuffleArray(positions, 4);
    for (int i = 0; i < ARRAY_SIZE(invaders); i++) {
      invaders[i].position = positions[i];
    }
  }
}

// Common function to print separator
void printStars(unsigned int number) {
  for (unsigned int i = 0; i < number; i++) {
    Serial.print("*");
  }
  Serial.println("");
}

// Ascii art for game title
void printBanner() {
  Serial.println("");
  printStars(75);
  Serial.println("                          _                     _               ");
  Serial.println("                         (_)                   | |              ");
  Serial.println(" ___ _ __   __ _  ___ ___ _ _ ____   ____ _  __| | ___ _ __ ___ ");
  Serial.println("/ __| '_ \\ / _` |/ __/ _ \\ | '_ \\ \\ / / _` |/ _` |/ _ \\ '__/ __|");
  Serial.println("\\__ \\ |_) | (_| | (_|  __/ | | | \\ V / (_| | (_| |  __/ |  \\__ \\");
  Serial.println("|___/ .__/ \\__,_|\\___\\___|_|_| |_|\\_/ \\__,_|\\__,_|\\___|_|  |___/");
  Serial.println("    | |");                                            
  Serial.println("    |_|");
  Serial.println("");
}

// help menu
void printHelp() {
  printBanner(); 
  Serial.println("Instructions:");
  Serial.println(" Destroy all space invaders!");
  Serial.println("");
  Serial.println(" Type your command to make a decoding guess.");
  Serial.println("");
  Serial.println("Game Commands:");
  Serial.println(" progress | progress report");
  Serial.println(" level    | level information");
  Serial.println(" help     | help commands");
  Serial.println(" hint     | clue");
  Serial.println(" reset    | reset game");
  Serial.println("");
  printStars(75);
  Serial.println("");
}

// progress report
void printProgress() {
  Serial.println("");
  printStars(75);
  Serial.println(" Progress");
  Serial.print(" Current Level: ");
  Serial.println(currentLevel);

  for (int i = 0; i < ARRAY_SIZE(puzzles); i++) {
    Serial.print(" Level ");
    Serial.print(puzzles[i].level);
    
    if(puzzles[i].complete == HIGH) {
      Serial.print(" complete with ");
    } else {
      Serial.print(" incomplete with ");
    }
    Serial.print(puzzles[i].guesses);
    Serial.println(" incorrect guesses.");
  } 
  
  printStars(75);
  Serial.println("");
}

// level report
void printLevel() {
  Serial.println("");
  printStars(75);;
  Serial.print(" Level:             ");
  Serial.print(currentLevel);
  Serial.println("");
  Serial.print(" Decryption Key:   ");
  Serial.println(puzzles[currentLevel].key);
  Serial.print(" Encoded Message:   ");
  Serial.println(puzzles[currentLevel].encodedMessage);
  Serial.print(" Incorrect Guesses: ");
  Serial.println(puzzles[currentLevel].guesses);
  printStars(75);
  Serial.println("");
}

void printHint(){
  Serial.println("");
  printStars(75);;
  Serial.print(" Level:             ");
  Serial.print(currentLevel);
  Serial.println("");
  Serial.print(" Hint: ");
  Serial.println(puzzles[currentLevel].hint);
  printStars(75);
  Serial.println("");
}

void printIncorrectGuess(char * guess, int guesses) {
  printStars(8);
  Serial.print(guess);
  Serial.print(" is an incorrect guess! You have tried ");
  Serial.print(guesses);
  Serial.println(" times.");
  Serial.println("");
}

void printCorrectGuess(char * guess, int guesses) {
  printStars(8);
  Serial.print(guess);
  Serial.print(" is correct!");
  Serial.print(" LEVEL ");
  Serial.print(currentLevel);
  Serial.print(" COMPLETE!");
  Serial.println("");

  printProgress();
  if(currentLevel < 3) {
    currentLevel += 1;
    printLevel();
  }
}

// Winner!
void checkForWin() {
  if(puzzles[0].complete && puzzles[1].complete && puzzles[2].complete && puzzles[3].complete) {
    Serial.println("");
    Serial.println(" YOU SAVED THE WORLD! ");
    Serial.println("         ________");
    Serial.println("     ,o88~~88888888o,");
    Serial.println("   ,~~?8P  88888     8,");
    Serial.println("  d  d88 d88 d8_88     b");
    Serial.println(" d  d888888888          b");
    Serial.println(" 8,?88888888  d8.b o.   8");
    Serial.println(" 8~88888888~ ~^8888\\ db 8");
    Serial.println(" ?  888888          ,888P");
    Serial.println("  ?  `8888b,_      d888P");
    Serial.println("   `   8888888b   ,888'");
    Serial.println("     ~-?8888888 _.P-~");
    Serial.println("          ~~~~~~");
    delay(10000);
    printHelp();
    reset();
  }
}

// process data (after user presses send/enter) from serial.
void process_data(char * data) {
  // load the help menu
  if(strcmp(data, "HELP") == 0) {
    printHelp();
    return;
  }

  // get a hint
  if(strcmp(data, "HINT") == 0) {
    printHint();
    return;
  }

  // reset the game
  if(strcmp(data, "RESET") == 0) {
    reset();
    return;
  }

  // show progress report
  if(strcmp(data, "PROGRESS") == 0) {
    printProgress();
    return;
  }

  // show level information
  if(strcmp(data, "LEVEL") == 0) {
    printLevel();
    return;
  }

  // compare the data to the current level secret and tally a correct or incorrect guess
  if(strcmp(data, puzzles[currentLevel].secret) == 0) {
    puzzles[currentLevel].complete = HIGH;
    printCorrectGuess(data, puzzles[currentLevel].guesses);
    return;
  } else {
    puzzles[currentLevel].guesses += 1;
    printIncorrectGuess(data, puzzles[currentLevel].guesses);
  }
} 

// process data from the serial monitor
void processIncomingByte (const byte inByte) {
  // how much serial data we expect before a newline
  const unsigned int MAX_INPUT = 50;

  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte){
    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data (strupr(input_line));

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1)) {
        input_line [input_pos++] = inByte;
      }
      break;
  }
}

void setup() {
  Serial.begin(9600);
  printHelp();

  randomSeed(analogRead(0));
  int devices = lc.getDeviceCount();

  for (int address = 0; address < devices; address++) {
    lc.shutdown(address, false);         // Wake up displays
    lc.setIntensity(address, intensity); // Set intensity levels
    lc.clearDisplay(address);            // Clear Displays
  }

  reset();
}

void loop(void) {
  // read the serial input
  while (Serial.available () > 0){
    processIncomingByte (Serial.read ());
  };

  // update the 'current time' for program state changes
  currentMillis = millis();

  updateInvaders();      // update the state of all of the invaders
  moveInvaders();        // make the invaders 'dance' or appear 'dead' and move the location on the LCD of the invader
  checkForWin();         // check if the game is complete
}
